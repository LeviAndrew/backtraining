import * as path from 'path';
import {TestManager} from '../TestManager';

const chai: any = require('chai');
const chaiHTTP = require('chai-http');
const config = require(path.resolve('devConfig.json'));

chai.use(chaiHTTP);
let expect = chai.expect;
let testManager = null;
const baseURL = `http://localhost:${config.server.port}`;

describe('3.Management - Papel', () => {

  let loggedUser;

  before((done) => {
    testManager = new TestManager(done);
  });

  describe('LOGIN', () => {

    it('Login labtic admin teste sucesso', (done) => {
    chai.request(baseURL)
        .post("/api/login")
        .send({
        login: 'labtic@admin.com',
        password: 'admin'
        })
        .end((error, response) => {
        expect(response.body).to.be.instanceof(Object);
        expect(response.body).to.have.all.keys("success", "data");
        expect(response.body.success).to.be.true;
        expect(response.body.data).to.be.instanceof(Object);
        expect(response.body.data).to.have.all.keys("cpf", "_id", "name", "surname", "email", "id", "accessKey", "authenticationKey");
        loggedUser = response.body.data;
        done();
        })
    });

  });

  describe("CRUD Papel", () => {

    let matriculaTurma, papel;

    describe("BEFORE", () => {

      it('Read matriculaTurmas', (done) => {
        chai.request(baseURL)
          .get("/api/user/matriculaTurma")
          .set('authentication-key', loggedUser.authenticationKey)
          .set('access-key', loggedUser.accessKey)
          .end((error, response) => {
            expect(response.body).to.be.instanceof(Object);
            expect(response.body).to.have.all.keys("success", "data");
            expect(response.body.success).to.be.true;
            expect(response.body.data).to.be.instanceOf(Object);
            expect(response.body.data).to.have.all.keys("matricula");
            expect(response.body.data.matricula).to.be.instanceOf(Array);
            response.body.data.matricula.forEach(matricula => {
              expect(matricula).to.be.instanceOf(Object);
              expect(matricula).to.have.all.keys("_id", "turma", "papel");
              expect(matricula.papel).to.be.instanceOf(Object);
              expect(matricula.papel).to.have.all.keys("_id", "acoes", "name", "id");
              expect(matricula.papel.acoes).to.be.instanceOf(Array);
              matricula.papel.acoes.forEach(acao => {
                expect(acao).to.be.instanceOf(Object);
                expect(acao).to.have.all.keys("_id", "metodos", "id", "name");
              });
            });
            matriculaTurma = response.body.data.matricula;
            done();
          });
      });

    })

    describe("Create Papel", () => {

      it('Criando papel professor: Sucesso!', (done) => {
        chai.request(baseURL)
          .post(`/api/management/${matriculaTurma[0].turma}/${matriculaTurma[0].papel.acoes[0].id}/${matriculaTurma[0].papel.acoes[0].metodos[0]}`)
          .set('authentication-key', loggedUser.authenticationKey)
          .set('access-key', loggedUser.accessKey)
          .send({
              name: "Professor",
              acoes: ["62ff7da2df5220363d1d6434"], // Criar papel (test)
          })
          .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body.data).to.be.instanceof(Array);
          response.body.data.forEach(papel => {
            expect(papel).to.be.instanceOf(Object);
            expect(papel).to.have.all.keys("id", "name", "removed", "acoes", "createdAt", "updatedAt");
            expect(papel.acoes).to.be.instanceOf(Array);
          });
          papel = response.body.data[0];
          done();
          })
      });

      // it('criando um novo usuário: sucesso!', (done) => {
      // chai.request(baseURL)
      //     .post("/api/user/create")
      //     .set('authentication-key', loggedUser.authenticationKey)
      //     .set('access-key', loggedUser.accessKey)
      //     .send({
      //         cpf: "3254165",
      //         name: "Teste",
      //         surname: "Calouros",
      //         email: "calouros@labtic.com",
      //         password: "calouros",
      //         telefone: "4825416945",
      //         endereco: "labtic"
      //     })
      //     .end((error, response) => {
      //     expect(response.body).to.be.instanceof(Object);
      //     expect(response.body.data).to.be.instanceof(Object);
      //     expect(response.body.data).to.have.all.keys("cpf", "_id", "name", "surname", "email", "id", "accessKey", "authenticationKey");
      //     loggedUser = response.body.data;
      //     done();
      //     })
      // });
        
    })

    describe("Read Papel", () => {

      it('Read All', (done) => {

      });

      it('Read By Id', (done) => {
        
      });
        
    })

    describe("Update Papel", () => {

      it('Atualiza nome do papel', (done) => {

      });

      it('Atualiza acoes do papel', (done) => {

      });
        
    })

    describe("Delete Papel", () => {

      it('Remove papel', (done) => {

      });

      it('Delete papel', (done) => {

      });
        
    })


  });

  describe("LOGOUT", () => {

    it('OK', (done) => {
    chai.request(baseURL)
        .post("/api/user/logout")
        .set('authentication-key', loggedUser.authenticationKey)
        .set('access-key', loggedUser.accessKey)
        .end((error, response) => {
        expect(response.body).to.be.instanceof(Object);
        expect(response.body).to.have.all.keys("success", "data");
        expect(response.body.success).to.be.true;
        expect(response.body.data).to.be.true;
        done();
        })
    });

  });

});