import * as path from 'path';
import {TestManager} from '../TestManager';

const chai: any = require('chai');
const chaiHTTP = require('chai-http');
const config = require(path.resolve('devConfig.json'));

chai.use(chaiHTTP);
let expect = chai.expect;
let testManager = null;
const baseURL = `http://localhost:${config.server.port}`;

describe('1.Open', () => {

  let loggedUser;

  before((done) => {
    testManager = new TestManager(done);
  });

  describe('LOGIN', () => {

    describe('TEST', () => {

      it('Login labtic admin teste sucesso', (done) => {
        chai.request(baseURL)
          .post("/api/login")
          .send({
            login: 'labtic@admin.com',
            password: 'admin'
          })
          .end((error, response) => {
            expect(response.body).to.be.instanceof(Object);
            expect(response.body).to.have.all.keys("success", "data");
            expect(response.body.success).to.be.true;
            expect(response.body.data).to.be.instanceof(Object);
            expect(response.body.data).to.have.all.keys("cpf", "_id", "name", "surname", "email", "id", "accessKey", "authenticationKey");
            loggedUser = response.body.data;
            done();
          })
      });

      it('Login com cpf', (done) => {
        chai.request(baseURL)
          .post("/api/login")
          .send({
            login: '9847946554',
            password: 'admin'
          })
          .end((error, response) => {
            expect(response.body).to.be.instanceof(Object);
            expect(response.body).to.have.all.keys("success", "data");
            expect(response.body.success).to.be.true;
            expect(response.body.data).to.be.instanceof(Object);
            expect(response.body.data).to.have.all.keys("cpf", "_id", "name", "surname", "email", "id", "accessKey", "authenticationKey");
            loggedUser = response.body.data;
            done();
          })
      });

    });

  });

  describe("LOGOUT", () => {

    describe('TEST', () => {

      it('OK', (done) => {
        chai.request(baseURL)
          .post("/api/user/logout")
          .set('authentication-key', loggedUser.authenticationKey)
          .set('access-key', loggedUser.accessKey)
          .end((error, response) => {
            expect(response.body).to.be.instanceof(Object);
            expect(response.body).to.have.all.keys("success", "data");
            expect(response.body.success).to.be.true;
            expect(response.body.data).to.be.true;
            done();
          })
      });

    });

    describe('AFTER', () => {

      it('LOGIN', (done) => {
        chai.request(baseURL)
          .post("/api/login")
          .send({
            login: 'admin@admin.com',
            password: 'admin'
          })
          .end((error, response) => {
            expect(response.body).to.be.instanceof(Object);
            expect(response.body).to.have.all.keys("success", "data");
            expect(response.body.success).to.be.true;
            expect(response.body.data).to.be.instanceof(Object);
            expect(response.body.data).to.have.all.keys("_id", "name", "surname", "email", "id", "accessKey", "authenticationKey");
            loggedUser = response.body.data;
            done();
          })
      });

    });

  });

});