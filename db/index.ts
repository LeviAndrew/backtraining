import {ManagerMap} from "../interfaces/ManagerMap";
import {
  User,
  Acao,
  Disciplina,
  Papel,
  Turma,
} from './Management'
import {
  Session
} from './Session'
/**
 * Inicia todos os managers.
 */
let Managers: ManagerMap = {
  user: new User(),
  session: new Session(),
  acao: new Acao(),
  disciplina: new Disciplina(),
  papel: new Papel(),
  turma: new Turma(),
};

export {Managers};