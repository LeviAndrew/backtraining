import {Schema} from "mongoose";

let schema_options = {
  toJSON: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      return ret;
    }
  },
  toObject: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      return ret;
    }
  }
};

const obj = {
  papel: {
    type: Schema.Types.ObjectId,
    ref: 'papel',
    required: true
  },
  turma: {
    type: Schema.Types.ObjectId,
    ref: 'turma',
    required: true
  }
}

  // @ts-ignore

let schema = new Schema(obj, schema_options);

export {schema};
