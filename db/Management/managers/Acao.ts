import {BasicManager} from "../../BasicManager";
import {Model} from "../model/Acao";

export class Acao extends BasicManager {
  wireCustomListeners () {}

  get model () {
    return Model;
  }

  get eventName () {
    return 'acao';
  }
}