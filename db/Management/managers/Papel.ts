import {BasicManager} from "../../BasicManager";
import {Model} from "../model/Papel";

export class Papel extends BasicManager {
  wireCustomListeners () {}

  get model () {
    return Model;
  }

  get eventName () {
    return 'papel';
  }
}