import {BasicManager} from "../../BasicManager";
import {Model} from "../model/Disciplina";

export class Disciplina extends BasicManager {
  wireCustomListeners () {}

  get model () {
    return Model;
  }

  get eventName () {
    return 'disciplina';
  }
}