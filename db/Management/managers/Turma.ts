import {BasicManager} from "../../BasicManager";
import {Model} from "../model/Turma";

export class Turma extends BasicManager {
  wireCustomListeners () {}

  get model () {
    return Model;
  }

  get eventName () {
    return 'turma';
  }
}