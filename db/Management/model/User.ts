import {model, Schema} from "mongoose";
import {BaseSchema} from "../../BaseSchema";
import {MatriculaTurma} from "../../subSchema";

let schema_options = {
  timestamps: true,
  toObject: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  },
  toJSON: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  }
};

// @ts-ignore

let schema = new Schema(Object.assign({
  cpf: {
    type: Schema.Types.String,
    trim: true,
    unique: true,
    required: [true, 'cpfRequired']
  },
  name: {
    type: Schema.Types.String,
    trim: true,
    required: true
  },
  surname: {
    type: Schema.Types.String,
    trim: true,
    required: true
  },
  email: {
    type: Schema.Types.String,
    required: true,
    unique: true,
    trim: true,
  },
  password: {
    type: Schema.Types.String,
    required: true,
    trim: true,
  },
  telefone: {
    type: Schema.Types.String,
    trim: true,
  },
  endereco: {
    type: Schema.Types.String,
    required: true,
    trim: true,
  },
  matricula: {
    type: [
      {
        type: MatriculaTurma,
      }
    ]
  },
  authenticationKey: {
    type: Schema.Types.String,
    required: true,
    unique: true,
    trim: true,
  }
}, BaseSchema), schema_options);

let modelSchema = model("user", schema);
export {modelSchema as Model};