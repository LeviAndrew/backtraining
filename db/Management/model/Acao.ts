import {model, Schema} from "mongoose";
import {BaseSchema} from "../../BaseSchema";

let schema_options = {
  timestamps: true,
  toObject: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  },
  toJSON: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  }
};

// @ts-ignore

let schema = new Schema(Object.assign({
  metodos: {
    type: [
        {
            type: String,
            trim: true,
            required: true
        },
    ],
  },
  name: {
    type: String,
    trim: true,
    unique: true,
    required: true
  }
}, BaseSchema), schema_options);

let modelSchema = model("acao", schema);
export {modelSchema as Model};