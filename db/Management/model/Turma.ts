import {model, Schema} from "mongoose";
import {BaseSchema} from "../../BaseSchema";

let schema_options = {
  timestamps: true,
  toObject: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  },
  toJSON: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  }
};

// @ts-ignore

let schema = new Schema(Object.assign({
  matriculados: {
    type: [
        {
            type: Schema.Types.ObjectId,
            ref: 'user',
            required: true,
            default: []
        },
    ],
  },
  name: {
    type: String,
    trim: true,
    required: true
  },
  vagas: {
    type: Number,
    required: true
  },
  dataInicio: {
    type: Date,
    required: true
  },
  dataFim: {
    type: Date,
    required: true
  },
  disciplina: {
    type: Schema.Types.ObjectId,
    ref: 'disciplina',
    required: true
  }
}, BaseSchema), schema_options);

let modelSchema = model("turma", schema);
export {modelSchema as Model};