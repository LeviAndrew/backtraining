import {model, Schema} from "mongoose";
import {BaseSchema} from "../../BaseSchema";

let schema_options = {
  timestamps: true,
  toObject: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  },
  toJSON: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  }
};

// @ts-ignore

let schema = new Schema(Object.assign({
  turmas: {
    type: [
        {
            type: Schema.Types.ObjectId,
            ref: 'turma',
            required: true
        },
    ],
  },
  name: {
    type: String,
    trim: true,
    required: true
  },
  ementa: {
    type: String,
    trim: true,
    required: true
  },
  cargaHoraria: {
    type: Number,
    required: true
  }
}, BaseSchema), schema_options);

let modelSchema = model("disciplina", schema);
export {modelSchema as Model};