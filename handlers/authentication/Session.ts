import {BasicHandler} from "../BasicHandler";
import {FindObject} from "../util/FindObject";

export class Session extends BasicHandler {

  public async checkPermission (param: permissionParam) {
    let required = this.attributeValidator([
      "auth", "aKey", "turmaId", "acao"
    ], param);
    if(!required.success) return await this.getErrorAttributeRequired(required.error);
    try {
      const userSession = await this.sendToServer('db.session.read', new FindObject({
        findOne: true,
        query: {
          _id: param.auth,
          accessKey: param.aKey,
        },
        populate: [
          {
            path: 'mainEntities.papel',
          }
        ]
      }));
      this.checkHubReturn(userSession.data);
      await Session.checkPapel({
        turmaId: param.turmaId,
        acao: param.acao,
        session: userSession.data.success.mainEntities,
      });
    } catch (e) {
      throw await this.returnHandler({
        model: 'session',
        data: {
          error: e.message || e,
        }
      });
    }
  }

  private static async checkPapel (param: papelParam) {
    for (let i = 0; i < param.session.length; i++) {
      if(param.turmaId === param.session[i].turmaId.toString()) {
        for (let j = 0; j < param.session[i].papel.acoes.length; j++) {
          if(param.acao === param.session[i].papel.acoes[j].toString()) return;
        }
      }
      if(param.session[i].mappedChildren.length) {
        for (let a = 0; a < param.session[i].mappedChildren.length; a++) {
          if(param.session[i].mappedChildren[a].toString() === param.turmaId) {
            for (let j = 0; j < param.session[i].papel.acoes.length; j++) {
              if(param.acao === param.session[i].papel.acoes[j].toString()) return;
            }
          }
        }
      }
    }
    throw new Error('methodUnauthorized');
  }

}

interface defaultParam {
  turmaId: string,
  acao: string,
}

interface permissionParam extends defaultParam {
  auth: string,
  aKey: string,
}

interface papelParam extends defaultParam {
  session: [any],
}

