import {BasicHandler} from "../BasicHandler";
import {FindObject} from "../util/FindObject";
import {UpdateObject} from "../util/UpdateObject";
import {Types} from "mongoose";

export class User extends BasicHandler {

  public async logout (param: logoutParam) {
    let required = this.attributeValidator(['auth', "aKey"], param);
    if(!required.success) return await this.getErrorAttributeRequired(required.error);
    try {
      let ret = await this.sendToServer('accessSession.removeKey', {
        authenticationKey: param.auth,
        accessKey: param.aKey,
      });
      return await this.returnHandler({
        model: 'user',
        data: ret.data,
      });
    } catch (e) {
      return await this.returnHandler({
        model: 'user',
        data: {error: e.message || e},
      })
    }
  }

  public async updatePassword (param: changePassword) {
    let required = this.attributeValidator([
      'auth', 'currentPassword', 'newPassword'
    ], param);
    if(!required.success) return await this.getErrorAttributeRequired(required.error);
    try {
      let userId: string = await this.getUserIdByAuth(param.auth);
      let checkPassword = await this.sendToServer('db.user.verifyPassword', {
        password: param.currentPassword,
        userId
      });
      if(!checkPassword.data.success || checkPassword.data.error) return await this.returnHandler({
        model: 'user',
        data: {error: 'invalidPassword'}
      });
      let ret = await this.sendToServer('db.user.update', new UpdateObject({
        query: userId,
        update: {
          password: param.newPassword,
        },
      }));
      let data = {success: null, error: null};
      if(!ret.data.success || ret.data.error) data.error = 'weCantUpdatePassword';
      else data.success = !!ret.data.success;
      return await this.returnHandler({
        model: 'user',
        data,
      });
    } catch (e) {
      return await this.returnHandler({
        model: 'user',
        data: {error: e.message || e},
      });
    }
  }

  public async matriculaTurmasRead (param: defaultParam) {
    let required = this.attributeValidator([
      'auth'
    ], param);
    if(!required.success) return await this.getErrorAttributeRequired(required.error);
    try {
      const userTurmaRet = await this.sendToServer('db.user.read', new FindObject({
          findOne: true,
          query: {
            authenticationKey: param.auth,
          },
          select: 'matricula',
          populate: [
            {
              path: 'matricula.papel',
              select: 'id acoes name',
              populate: [
                {
                  path: 'acoes',
                  select: 'id metodos name'
                }
              ]
            }
          ]
      }))
      for (let i = 0; i < userTurmaRet.length; i++) {
        this.checkHubReturn(userTurmaRet[i].data);
      }
      return await this.returnHandler({
        model: 'user',
        data: {success: userTurmaRet.data.success},
      });
    } catch (e) {
      return await this.returnHandler({
        model: 'user',
        data: {error: e.message || e},
      });
    }
  }

}

export default new User();

interface defaultParam {
  auth: string,
}

interface logoutParam extends defaultParam {
  aKey: string,
}

interface changePassword extends defaultParam {
  currentPassword: string,
  newPassword: string,
}