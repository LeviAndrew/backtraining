import {BasicHandler} from "../BasicHandler";
import {FindObject} from "../util/FindObject";
import {UpdateObject} from "../util/UpdateObject";
import {QueryObject} from "../util/QueryObject";

export class Management extends BasicHandler {

    public async papelCreate(param: defaultParam<papelCreate>) {
      let required = this.attributeValidator([
        "auth", "aKey", "data", [
          "name", "acoes"
        ]
      ], param);
      if (!required.success) return await this.getErrorAttributeRequired(required.error);
      try {
        const papelRet = await this.sendToServer('db.papel.create', {
          name: param.data.name,
          acoes: param.data.acoes,
        });
        return await this.returnHandler({
          model: 'papel',
          data: papelRet.data,
        });
      } catch (e) {
        return await this.returnHandler({
          model: 'papel',
          data: {error: e.message || e},
        });
      }
    }


}

export default new Management();

interface defaultParam<T> {
  auth: string,
  aKey: string,
  turmaId: string,
  data: T,
}

interface papelCreate {
  name: string,
  acoes: [string]
}