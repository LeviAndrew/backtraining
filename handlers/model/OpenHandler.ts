import {BasicHandler} from "../BasicHandler";
import {FindObject} from '../util/FindObject';
import {Application} from '../../Application';
import * as path from 'path';

export class OpenHandler extends BasicHandler {

  public async getLocale (data) {
    let dataLocale = Application.getPathI18N();
    let i18n = data.i18n ? data.i18n : dataLocale.defaultI18N;
    let ret = await this.getI18N({path: path.resolve(`${dataLocale.mainPath}/${i18n}/${dataLocale.i18n}`)});
    return await this.returnHandler({
      model: 'global',
      data: ret
    })
  }

  public async login (data: loginData): Promise<returnData> {
    let required = this.attributeValidator(['login', 'password'], data), ret;
    if(!required.success) return await this.getErrorAttributeRequired(required.error);
    try {
      ret = await this.sendToServer('db.user.login', {
        password: data.password,
        queryObject: new FindObject({
          query: {
            $or:[
              {
                email: data.login,
              },
              {
                cpf: data.login,
              },
            ],
            removed: false,
          },
          select: 'id name cpf surname email authenticationKey ',
        }),
      });
      if(ret.data.error) throw new Error('emailOrPasswordInvalid');
      let accessKey = await this.sendToServer('accessSession.create', ret.data.success.authenticationKey);
      if(accessKey.data.error) return await this.returnHandler({
        model: 'user',
        data: accessKey.data,
      });
      ret.data.success.accessKey = accessKey.data.success;
      return await this.returnHandler({
        model: 'user',
        data: ret.data,
      });
    } catch (e) {
      return await this.returnHandler({
        model: 'user',
        data: {error: e.message || e},
      })
    }
  }

  public async userCreate2 (param: userCreate): Promise<returnData> {
    try {
      let ret = await this.sendToServer('db.user.create', param.data);
      console.log(ret);
      this.checkHubReturn(ret.data);
      return await this.returnHandler({
        model: 'user',
        data: ret.data,
      });
    } catch (e) {
      return await this.returnHandler({
        model: 'user',
        data: {error: e.message || e},
      })
    }
  }

}

interface returnData {
  success: boolean,
  data: any
}

interface loginData {
  login: string,
  password: string
}

interface userCreate {
  data: {
    cpf: string,
    name: string,
    surname: string,
    email: string,
    password: string,
    telefone: string,
    endereco: string
  }
}

export default new OpenHandler();